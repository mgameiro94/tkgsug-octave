## Copyright (C) 2018 Miguel Gameiro
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} createTkgRule (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Miguel Gameiro <miguel@miguel-HP-ENVY-Notebook>
## Created: 2018-10-18

function [tkgRule] = createTkgRule (tkgnet, ruletype, numClust, combInd)
  
  % Verify if type of rule is valid
  switch (ruletype)
    case 'AND'
      W = combnk(1:numClust,combInd);
    case 'OR'
      W = combnk(1:numClust,combInd);
    otherwise
      error('Invalid rule type!');
  endswitch
  
  % Create the layer
  field1 = 'type'; value1 = 'rule'; 
  field2 = 'ruletype'; value2 = ruletype;
  field3 = 'combInd'; value3 = combInd;
  field4 = 'W'; value4 = 0;
  tkgRule = struct(field1,value1,field2,value2,field3,value3,field4,value4);
  
  % Init matrix values
  tkgRule.W = W;
  
endfunction
