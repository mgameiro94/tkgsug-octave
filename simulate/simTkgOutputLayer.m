## Copyright (C) 2018 Miguel Gameiro
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} simTkgOutputLayer (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Miguel Gameiro <Miguel Gameiro@DESKTOP-8PK9G1U>
## Created: 2018-11-17

function Yk = simTkgOutputLayer (tkgnet, Xk, ln)

  % Output matrix initialization
  Yk = zeros(tkgnet.info.numOutputs,1);
  
  % Calculate for every output
  for n = 1:tkgnet.info.numOutputs
    Yk(n,:) = sum(Xk.*tkgnet.layers{ln}.W(:,n));
  endfor
  
endfunction
