## Copyright (C) 2018 Miguel Gameiro
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} createTkg (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Miguel Gameiro <miguel@miguel-MS-7A72>
## Created: 2018-10-17

function tkgnet = createTkg (mftype, nummf, normmf)
  
  % Create the main structure
  field1 = 'info'; value1 = 0;
  field2 = 'layers'; value2 = 0;
  field3 = 'trainParam'; value3 = 0;
  tkgnet = struct(field1,value1,field2,value2,field3,value3);
    
  % Initialize the info
  tkgnet.info = createTkgInfo(3, 3, 1);
  
  % Initialize layers
  tkgnet.layers = cell(3,1);
  tkgnet.layers{1} = createTkgMF(tkgnet, mftype, nummf, normmf);
  tkgnet.layers{2} = createTkgRule(tkgnet, 'AND', tkgnet.layers{1}.numClust, 2);
  tkgnet.layers{3} = createTkgWeights(tkgnet, tkgnet.layers{1}.numClust, 2);
  
  % Initialize train parameters
  tkgnet.trainParam = createTkgTrainParam('mse', 'gradient');
  
endfunction
