## Copyright (C) 2018 Miguel Gameiro
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} simTkgMflayer (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Miguel Gameiro <Miguel Gameiro@DESKTOP-8PK9G1U>
## Created: 2018-11-17

function Yk = simTkgMfLayer (tkgnet, Xk, ln)
  
    switch(tkgnet.layers{ln}.mftype)
      case 'triangle'
        Yk = cell(numel(Xk),1);
        for kn = 1:numel(Xk)
          % Output from the actual layer
          Kcl = tkgnet.layers{ln}.mfcell{kn};
          y = zeros(numel(Kcl(:,1)),1);
          % For each variable get the cluster degree
          for n = 1:numel(Kcl(:,1))
              % Triangle function of clustering, using piecewise equations
              % Bellow cluster min limit
              if (Xk(kn) <= Kcl(n,1))
                  if (n == 1)
                      y(n) = 1;
                  else
                      y(n) = 0;
                  end
              % Bellow cluster average value
              elseif (Xk(kn) <= Kcl(n,2))
                  if (n == 1)
                      y(n) = 1;
                  else
                      m = (1 - 0)/(Kcl(n,2) - Kcl(n,1));
                      b = 1 - m*Kcl(n,2);
                      y(n) = m*Xk(kn) + b;
                  end
              % Above cluster average value
              elseif (Xk(kn) <= Kcl(n,3))
                  if (n == numel(Kcl(:,1)))
                      y(n) = 1;
                  else
                      m = (0 - 1)/(Kcl(n,3) - Kcl(n,2));
                      b = 1 - m*Kcl(n,2);
                      y(n) = m*Xk(kn) + b;
                  end
              % Above cluster max limit
              elseif (Xk(kn) >= Kcl(n,3))
                  if (n == numel(Kcl(:,1)))
                      y(n) = 1;
                  else
                      y(n) = 0;
                  end
              end
              Yk{kn} = y;
          end
       endfor
       % Convert it again to a matrix
       Yk = cell2mat(Yk);
      otherwise
        error('Membership-function not implemented!');
    endswitch
  
endfunction
