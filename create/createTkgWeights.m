## Copyright (C) 2018 Miguel Gameiro
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} createTkgWeights (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Miguel Gameiro <miguel@miguel-HP-ENVY-Notebook>
## Created: 2018-10-18

function [tkgW] = createTkgWeights (tkgnet, numClust, combInd)
  
  % Create the base structure
  field1 = 'type'; value1 = 'output';
  field2 = 'W'; value2 = 0;
  tkgW = struct(field1,value1,field2,value2);
  
  % Allocate the weights matrix
  tkgW.W = zeros(nchoosek(numClust,combInd),1);
  
endfunction
