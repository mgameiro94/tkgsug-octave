## Copyright (C) 2018 Miguel Gameiro
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} simTkg (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Miguel Gameiro <Miguel Gameiro@DESKTOP-8PK9G1U>
## Created: 2018-11-11

function Y = simTkg (tkgnet, X)
  
  % Allocate the aswer matrix
  Y = zeros(tkgnet.info.numOutputs,numel(X(1,:)));
  
  % Scroll for every instant
  for k = 1:numel(X(1,:))
    % Get the matrix on each instant
    Xk = X(:,k);
    % Scroll for evey layer
    for ln = 1:tkgnet.info.numLayers
      Xk = simTkgLayer(tkgnet, Xk, ln);
    endfor
    % Concatenate this instant response
    Y(:,k) = Xk;
  endfor
  
endfunction
