## Copyright (C) 2018 Miguel Gameiro
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} createMFLayer (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Miguel Gameiro <miguel@miguel-MS-7A72>
## Created: 2018-10-17

function tkgmf = createTkgMF (tkgnet, mftype, numMF, normMF)
 
  % Create structure as function of MF
  switch(mftype)
    case 'triangle'
      % Allocate default cell and matrices
      mfcell = cell(tkgnet.info.numInputs,1);
      mfSize = zeros(numel(numMF),1);
      % Initialize values for mf layer
      for n = 1:tkgnet.info.numInputs
        % Calculate size of the cluster
        mfSize(n) = (normMF(n,2)-normMF(n,1))/(numMF(n)-1);
        % Original matrices
        Kc = zeros(numMF(n),1);
        Kl = zeros(numMF(n),2);
        % MF centroids
        Kc(1) = normMF(n,1);
        for m = 2:numMF(n)-1
            Kc(m) = normMF(n,1) + (m-1)*mfSize(n);
        end
        Kc(end) = normMF(n,2);
        % MF limits
        Kl(1,1) = normMF(n,1);
        Kl(1,2) = Kc(2);
        for m = 2:numMF(n)-1
            Kl(m,1) = Kc(m-1);
            Kl(m,2) = Kc(m+1);
        end
        Kl(end,1) = Kc(end-1);
        Kl(end,2) = normMF(n,2);
        % Store the values
        mfcell{n} = [Kl(:,1) Kc Kl(:,2)];
      endfor
    case 'gaussian'
      mfmat = [1 0; 1 0.5; 1 1];
    case 'trapezoid'
      mfmat = [-1 0 0.2 0.4; 0.2 0.4 0.6 0.8; 0.6 0.8 1.0 1.1];      
    otherwise
      error('Unknown type of membership-function!');
  endswitch 
 
  % Create type of layer field
  field1 = 'type'; value1 = 'mflayer';
  field2 = 'mftype'; value2 = mftype;
  field3 = 'numClust'; value3 = 0;
  field4 = 'numMF'; value4 = 0;
  field5 = 'normMF'; value5 = 0;
  field6 = 'mfcell'; value6 = 0; 
  
  % Create the structure
  tkgmf = struct(field1,value1,field2,value2,field3,value3,field4,value4,field5,value5,field6,value6);
  
  % Init the values
  tkgmf.numClust = sum(numMF);
  tkgmf.numMf = numMF;
  tkgmf.normMF = normMF;
  tkgmf.mfcell = mfcell;
  
endfunction