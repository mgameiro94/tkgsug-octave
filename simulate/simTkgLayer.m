## Copyright (C) 2018 Miguel Gameiro
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} simTkgLayer (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Miguel Gameiro <Miguel Gameiro@DESKTOP-8PK9G1U>
## Created: 2018-11-17

function Yk = simTkgLayer (tkgnet, Xk, ln)
  
  % Choose different functions depending on type of layer
  switch(tkgnet.layers{ln}.type)
    case 'mflayer'
      Yk = simTkgMfLayer(tkgnet, Xk, ln);
    case 'rule'
      Yk = simTkgRuleLayer(tkgnet, Xk, ln);
    case 'output'
      Yk = simTkgOutputLayer(tkgnet, Xk, ln);
    otherwise
      error('Unknown type of layer!');
  endswitch
endfunction
