## Copyright (C) 2018 Miguel Gameiro
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} createTkgInfo (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Miguel Gameiro <miguel@miguel-MS-7A72>
## Created: 2018-10-17

function tkgInfo = createInfo (numLayers, numInputs, numOutputs)
  
  % Create base info structure
  field1 = 'numLayers'; value1 = 0;
  field2 = 'numInputs'; value2 = 0;
  field3 = 'numOutputs'; value3 = 0;
  tkgInfo = struct(field1,value1,field2,value2,field3,value3);
  
  % Initialize variables
  tkgInfo.numLayers = numLayers;
  tkgInfo.numInputs = numInputs;
  tkgInfo.numOutputs = numOutputs;
  
endfunction
