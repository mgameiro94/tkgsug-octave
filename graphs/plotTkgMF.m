## Copyright (C) 2018 Miguel Gameiro
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} plotTkgMF (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Miguel Gameiro <miguel@miguel-MS-7A72>
## Created: 2018-10-17

function plotTkgMF (tkg, nlayer)
  
  % Differ the fuzzylayer from the others
  switch(tkg.layers{nlayer}.type)
    case 'mflayer'
      % Scroll for every layer in the net
      for n = 1:tkg.layers{nlayer}.numClust
        % plot as a function of the type of layer
        switch(tkg.layers{nlayer}.mftype)
          case 'triangle'
            % Plot the membership-functions
            plot(0:0.001:1,trimf(0:0.001:1,tkg.layers{nlayer}.mfmat(n,:)));
            hold on;
          case 'gaussian'
            % Plot the membership-functions
            plot(0:0.001:1,gaussmf(0:0.001:1,tkg.layers{nlayer}.mfmat(n,:)));
            hold on;
          case 'trapezoid'
      
          case 'sig'
      
          case 'S'
      
          case 'Z'
            
          otherwise
            error('Unknown type of membership-function!');
        endswitch
      endfor
    otherwise
      error('Not a fuzzy membership-function layer!');
    endswitch
    
endfunction
