%% Load toolboxes
pkg load statistics

%% Add the necessary paths
addpath('create');
addpath('graphs');
addpath('simulate');


%% Create the strcture
tkg = createTkg('triangle', [3; 3; 3], [0 1; 0 1; 0 1]);


%% Simulate the Tkagi-Sugeno model
X = ones(3,1e4);
Y = simTkg(tkg, X);